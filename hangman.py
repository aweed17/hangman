#hangman game, written by Aaron Weed and Bill Luo
import random
#from dictionarylist import *
wordlist = open('hangman-words.txt').readlines() #Import wordlist
wordlist = [i.replace('\n','') for i in wordlist] #Get rid of all the space.
secret_word = random.choice(wordlist).upper() #Generate a random word from wordlist.
gameIsDone = False #Game is not finished yet.
#Show hangman board.
hangman_board = ['''
    /-----\\
          |
          |
          |
          |
          |
    ------------''','''
    /-----\\
    o     |
          |
          |
          |
          |
    ------------''','''
    /-----\\
    o     |
    |     |
          |
          |
          |
    ------------''','''
    /-----\\
    o     |
   \|/    |
          |
          |
          |
    ------------''','''
    /-----\\
    o     |
   \|/    |
    |     |
          |
          |
    ------------''','''
    /-----\\
    o     |
   \|/    |
    |     |
   /      |
          |
    ------------''','''
    /-----\\
    o     |
   \|/    |
    |     |
   / \    |
          |
    ------------'''] #The hangman board

correct = [] #Letter that user guesses right.
incorrect = [] #Letter that user guesses wrong.
score = 0 #User score

#print('Testing: %s' % secret_word) #Test
print('Welcome to the game Hangman.') #Instruction
print('Here is the instruction for our game.')
print('You have five guesses before the man is hanged. Remember Space is included our secret word.')
print('Good Luck!')

def draw():
    print(hangman_board[len(incorrect)]) #Corresponding hangman draw.
    print('Your word has %s letters.' % len(secret_word)) #Indicate how many letter the word has.
    for i in secret_word:
        if i in correct:
            print(i, end=' ') #Seperate each letter with a space.
        else:
            print ('_', end=' ') #Use line to represent each letter.
    print ('\n\nMissed Letters')
    for i in incorrect:
        print(i,end=' ')
    print('\n*******************')

def user_guess():
    while True:
        guess = input('Guess a letter\n: ').upper() #Get user input.
        if guess in correct or guess in incorrect: #If user is already in correct or incorrect list.
            print('You have already guessed that letter. Guess again.')
        elif guess.isnumeric(): #If user input is a number.
            print('Please enter only letters, not numbers. Guess again.')
        elif len(guess) > 1: #If user inputs more than one letter.
            print("Please enter only one letter at a time. Guess again.")
        elif len(guess) == 0: #If user inputs nothing.
            print('Please enter your selections.')
        else:
            break
    if guess in secret_word: #If userguess is one letter in secret_word
        correct.append(guess)
    else: #If userguess is not a letter in secret_word
        incorrect.append(guess)

def check_win():
    if len(incorrect) > 5: #User has five guesses, if there are over 5 letters in incorrect list, user loses the game.
        return 'loss' #User loses
    for i in secret_word:
        if i not in correct:
            return 'no win'
    return 'win' #User wins

def playagain(): #The function returns True if the player wants to play again.
    print('Do you want to play again? (yes or no)')
    return input().lower().startswith('y')
while True:
    draw() #Call draw function
    user_guess() #Call user_guess function
    win_condition = check_win() #Set win_condition to check_win
    if win_condition == 'loss': #If user gets it wrong.
        print('Game Over. The word was %s.' % secret_word)
        print ('You score is %s' % score)
        gameIsDone = True
    elif win_condition == 'win': #If user gets it right.
        print ('You win. The word was %s' % secret_word)
        score = score + 1
        print ('You score is %s' % score)
        gameIsDone = True
    if gameIsDone:
        if playagain(): #If user wants to play again, reset correct and incorrect list, set gameIsDone False, and generate a random word.
            correct = []
            incorrect = []
            gameIsDone = False
            secret_word = random.choice(wordlist).upper()
        else: #If user doesn't want to play again.
            break #Exit.

